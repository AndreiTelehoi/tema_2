function addTokens(input, tokens) {
    if (typeof input !== "string") {
        throw new Error("Invalid input");
    }

    if (input.length < 6) {
        throw new Error("Input should have at least 6 characters");
    }

    for (let i = 0; i < tokens.length; i++) {
        if (typeof tokens[i].tokenName !== "string") {
            throw new Error("Invalid array format");
        }
    }

    if (input.includes('...') === false) {
        return input;
    } else {

        let result;
        for (let i = 0; i < tokens.length; i++) {
            result = input.replace("...", "${" + tokens[i].tokenName + "}");
        }
        return result;
    }




}

const app = {
    addTokens: addTokens
}

module.exports = app;